﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace assignment2
{
    public partial class main : Form
    {
        String user, user_id, developer_name, send, search_key;
        int complete, bug_id, department, user_department;
        MySqlConnection connection = new MySqlConnection("server = 127.0.0.1; user id = root; persistsecurityinfo = True; database = assignment2"); // connect the database
        DataGridViewRow row;
        MySqlDataAdapter adapter;
        MySqlCommand cmd;
        Dictionary<int, string> comboSource;
        ArrayList array, bug_detail;
        Microsoft.Office.Interop.Excel.Application Excel;
        Label[] label, detail_label;
        int n = 9;
        int space = 20;
        int space2 = 20;

        public main()
        {
            InitializeComponent();
            user = user_name.Text;
            user_id = id.Text;
            this.bugdetail.Focus();
            timer1.Start();
        } // save the login information to this form 

        public main(string user, string user_id, Int32 user_department)
        {
            InitializeComponent();
            user_name.Text = user;
            id.Text = user_id;
            department = user_department;
        } // get login information

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.Controls.Clear();
            Form1 form = new Form1();
            form.TopLevel = false;
            form.FormBorderStyle = FormBorderStyle.None;
            this.Controls.Add(form);
            form.Show();
        } // this is the login out function

        private void main_Load(object sender, EventArgs e)
        {
            searchRequest.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
            complete = 0;
            show_department();
            data();
            
            bugdetail.AutoScroll = true;
            panel5.AutoScroll = true;

            this.time.Text = DateTime.Now.ToShortTimeString();

            if (department == 2)
            {
                this.new_bug.Visible = false;
                this.returnToQA.Visible = true;
                this.assigns.Visible = true;
                this.update.Visible = false;
                this.finish.Visible = false;
                this.dm_plugin.Visible = true;
            }else if (department == 3)
            {
                this.new_bug.Visible = false;
                this.returnToQA.Visible = true;
                this.assigns.Visible = false;
                this.update.Visible = false;
                this.finish.Visible = false;
            } else
            {
                this.dm_plugin.Visible = false;
            }
        } // this is show the each user default interface

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.time.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        }// show the system date and time

        private void show_department()
        {
            if (department == 3)
            {
                adapter = new MySqlDataAdapter("select * from assignment2.bug where complete = '" + complete + "' and department = '3'", connection);
            }
            else if (department == 2)
            {
                adapter = new MySqlDataAdapter("select * from assignment2.bug where complete = '" + complete + "' and department = '2' or department ='3'", connection);
            }
            else
            {
                adapter = new MySqlDataAdapter("select * from assignment2.bug where complete = '" + complete + "'", connection);
            }
        } // each user show different data

        private void buttom_visible()
        {
            this.finish.Visible = false;
            this.update.Visible = false;
            this.returnToQA.Visible = true;
            this.assigns.Visible = true;
        } // control user can use witch button

        private void data()
        {
            connection.Open();
            DataSet ds = new DataSet();
            adapter.Fill(ds, "bug");
            dataGridView1.DataSource = ds.Tables["bug"];

            this.dataGridView1.Columns[3].Visible = false;
            this.dataGridView1.Columns[4].Visible = false;
            this.dataGridView1.Columns[5].Visible = false;
            this.dataGridView1.Columns[9].Visible = false;
            this.dataGridView1.Columns[9].Visible = false;
            this.dataGridView1.Columns[10].Visible = false;
            this.dataGridView1.Columns[12].Visible = false;
            this.dataGridView1.Columns[13].Visible = false;

            dataGridView1.AllowUserToAddRows = false;

            connection.Close();
        } // connect the database and input the data to data view

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox2.SelectedIndex)
            {
                case 0:
                    complete = 0;

                    show_department();
                    data();

                    break;
                case 1:
                    complete = 1;
                    adapter = new MySqlDataAdapter("select * from assignment2.bug where complete = '" + complete + "'", connection);

                    data();
                    dataGridView1.Columns[12].Visible = true;
                    dataGridView1.Columns[13].Visible = true;
                    break;
            }
            this.bugdetail.Visible = false;
        } // show the incomplete or complete bug report

        private void dataClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                row = this.dataGridView1.Rows[e.RowIndex];
                if (comboBox2.SelectedIndex == 0)
                {
                    complete_data();

                    if (department == 3)
                    {
                        this.bugFreeback.ReadOnly = true;
                        if (user_name.Text != row.Cells["developer_name"].Value.ToString())
                        {
                            detail_readonly();
                        } else
                        {
                        edit_detail();

                        this.finish.Visible = false;
                        this.update.Visible = false;
                        this.returnToQA.Visible = true;
                        this.assigns.Visible = false;
                        }
                        
                    }
                    else if (department == 2)
                    {
                        this.bugFreeback.ReadOnly = true;
                        edit_detail();
                        this.dm_plugin.Visible = true;
                        comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;

                        this.finish.Visible = false;
                        this.update.Visible = false;
                        this.returnToQA.Visible = true;
                        this.assigns.Visible = true;
                    } else
                    {
                        this.bugFreeback.ReadOnly = false;
                        edit_detail();

                        this.finish.Visible = true;
                        this.update.Visible = true;
                        this.returnToQA.Visible = false;
                        this.assigns.Visible = false;
                    }
                    
                } else
                {
                    detail_readonly();
                }
                
            }
            
            this.panel5.Visible = false;
            this.bugdetail.Visible = true;
            this.bugdetail.Focus();

            bugser.DropDownStyle = ComboBoxStyle.DropDownList;
            
        } // double click the data view can show the bug report detail 
        
        private void new_bug_Click(object sender, EventArgs e)
        {
            this.panel5.Visible = true;
            this.bugdetail.Visible = false;
            newBugSeverity.DropDownStyle = ComboBoxStyle.DropDownList;
        } // (end new bug)  open the new bug report panel

        private void back_Click(object sender, EventArgs e)
        {
            this.panel5.Visible = false;
            this.bugdetail.Visible = true;
            reset_data();
        } // (end back)  close the new bug report panel and reset the text view

        private void reset_Click(object sender, EventArgs e)
        {
            reset_data();
        } // (end reset)  reset the text view

        private void submit_Click(object sender, EventArgs e)
        {
            if (newBugDescription.Text == "" || newBugSymptom.Text == "" || newBugTitle.Text == "" || newBugReproduction.Text == "" || newBugSummary.Text == "")
            {
                DialogResult dr = MessageBox.Show("Please enter all text!!", "Warning", MessageBoxButtons.OK); // show the warning message
            }
            else
            {
                string sql = "select max(bug_id) from assignment2.bug"; // get the max bug id number
                connection.Open();
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                string bugID = cmd.ExecuteScalar().ToString();
                bug_id = Int32.Parse(bugID) + 1;

                string insert = "insert into assignment2.bug (bug_id, title, severity, symptom, description, department, start_date) values ('" +
                bug_id + "','" + newBugTitle.Text + "','" + newBugSeverity.Text + "','" + newBugSymptom.Text +
                "','" + newBugDescription.Text + "','2', '" + DateTime.Now.ToString("dd/MM/yyyy") + "');"; // insert sql code
                MySqlCommand cmd1 = new MySqlCommand(insert, connection);
                cmd1.ExecuteNonQuery();
                connection.Close();
                data();
                DialogResult dr = MessageBox.Show("Submit Complete!!", "Complete", MessageBoxButtons.OK); // show the success message
            } // end if
        } // (end submit)  QA submit the new bug report to development manager

        private void update_Click(object sender, EventArgs e)
        {
            int version = Int32.Parse(bugver.Text) + 1;
            String update = "update assignment2.bug set title ='" + bugtitle.Text + "', build_version = '"
                + version + "' , description = '" + bugD.Text + "' , severity = '" + bugser.Text + 
                "', reproduction = '" + bugR.Text + "' , summary = '" + bugS.Text +
                "', freeback = '" + bugFreeback.Text + "', symptom = '" + bugSymptom.Text + 
                "', department = '2' where bug_id = '" + bugid.Text + "' ;"; // update sql code

            connection.Open();
            MySqlCommand cmd = new MySqlCommand(update, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
            
            data();
            
            DialogResult dr = MessageBox.Show("Submit Complete!!", "Complete", MessageBoxButtons.OK); // show the submit massage
        } // (end sned)  QA send the bug report to development manager

        private void finish_Click(object sender, EventArgs e)
        {
            complete = 1;
            string bug_complete = "update assignment2.bug set complete ='" + complete + "', end_date ='" + DateTime.Now.ToString("dd/MM/yyyy") + "' where bug_id = '" + bugid.Text + "';";
            connection.Open();
            MySqlCommand cmd = new MySqlCommand(bug_complete, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
            complete = 0;
            data();
            this.bugdetail.Visible = false;

            DialogResult dr = MessageBox.Show("Close the bug report!!", "Complete", MessageBoxButtons.OK);
        }// submit the complete bug report

        private void returnToQA_Click(object sender, EventArgs e)
        {
            user_department = 1;

            send = "update assignment2.bug set title ='" + bugtitle.Text + "', build_version = '"
                + bugver.Text + "' , severity = '" + bugser.Text + "' , description = '" + bugD.Text + "', reproduction = '" + bugR.Text + "' , summary = '" + bugS.Text +
                "', symptom = '" + bugSymptom.Text + "', department = '" + user_department + "' where bug_id = '" + bugid.Text + "';"; // update sql code

            update_data();
            DialogResult dr = MessageBox.Show("Return Complete!!", "Complete", MessageBoxButtons.OK); // show the success massage
        } // (end return)  development manager and developer can return the bug report to QA

        private void assigns_Click(object sender, EventArgs e)
        {
            user_department = 3;
            developer_name = comboBox1.SelectedValue.ToString();

            send = "update assignment2.bug set title ='" + bugtitle.Text + "', build_version = '"
                + bugver.Text + "' , severity = '" + bugser.Text + "' , description = '" + bugD.Text + "', reproduction = '" + bugR.Text + "' , summary = '" + bugS.Text +
                "', symptom = '" + bugSymptom.Text + "', department = '" + user_department +
                "', developer_name ='" + developer_name + "' where bug_id = '" + bugid.Text + "';"; // update sql code

            update_data();
            DialogResult dr = MessageBox.Show("Assigns Complete!!", "complete", MessageBoxButtons.OK); // show the success message
        } // (end assigns)  development mamager send the bug report to developer 

        private void serachbtn_Click(object sender, EventArgs e)
        {
                if (comboBox2.SelectedIndex == 0)
                {
                    switch (searchRequest.Text)
                    {
                        case "Bug ID":
                            search_key = "bug_id";
                            break;
                        case "Title":
                            search_key = "title";
                            break;
                        case "Severity":
                            search_key = "severity";
                            break;
                        case "Developer Name":
                            search_key = "developer_name";
                            break;

                    }// change the combo box text to database column name
                    if (department == 1)
                    {
                        adapter = new MySqlDataAdapter("select * from assignment2.bug where (" + search_key + " like '%" + txtserach.Text + "%') and complete ='" + 0 + "';", connection);
                    }
                    else if (department == 2)
                    {
                        adapter = new MySqlDataAdapter("select * from assignment2.bug where (" + search_key + " like '%" + txtserach.Text + "%') and complete ='" + 0 + "' and department = '2' or department ='3';", connection);
                    }
                    else
                    {
                        adapter = new MySqlDataAdapter("select * from assignment2.bug where (" + search_key + " like '%" + txtserach.Text + "%') and complete ='" + 0 + "' and department = '3';", connection);
                    }
                } // each user show different data
                else
                {
                    adapter = new MySqlDataAdapter("select * from assignment2.bug where (" + search_key + " like '%" + txtserach.Text + "%') and complete ='" + 1 + "';", connection);

                } // show the complete bug report search result
                data(); // input data function
        } // (end search)  the search function

        private void keydown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (comboBox2.SelectedIndex == 0)
                {
                    switch (searchRequest.Text)
                    {
                        case "Bug ID" :
                            search_key = "bug_id";
                            break;
                        case "Title":
                            search_key = "title";
                            break;
                        case "Severity":
                            search_key = "severity";
                            break;
                        case "Developer Name":
                            search_key = "developer_name";
                            break;

                    }
                    if (department == 1)
                    {
                        adapter = new MySqlDataAdapter("select * from assignment2.bug where (" + search_key + " like '%" + txtserach.Text + "%') and complete ='" + 0 + "';", connection);
                    } else if (department ==2)
                    {
                        adapter = new MySqlDataAdapter("select * from assignment2.bug where (" + search_key + " like '%" + txtserach.Text + "%') and complete ='" + 0 + "' and department = '2' or department ='3';", connection);
                    }
                    else
                    {
                        adapter = new MySqlDataAdapter("select * from assignment2.bug where (" + search_key + " like '%" + txtserach.Text + "%') and complete ='" + 0 + "' and department = '3';", connection);
                    }
                    

                }
                else
                {
                    adapter = new MySqlDataAdapter("select * from assignment2.bug where (" + search_key + " like '%" + txtserach.Text + "%') and complete ='" + 1 + "';", connection);

                }
                data();
            }
        } // (end keydown)  this is key down function for search

        private void reflash_Click(object sender, EventArgs e)
        {
            show_department();
            data(); // show the data
        } // (end reflash)  reflash the data

        private void generate_report_Click(object sender, EventArgs e)
        {
            bug_report.AutoScroll = true;
            bug_report.Visible = true;
            dataGridView1.Visible = false;
            bugreportsearch.Visible = false;
            txtserach.Visible = false;
            serachbtn.Visible = false;
            reflash.Visible = false;
            generate_report.Visible = false;
            searchRequest.Visible = false;
            comboBox2.Text = "Complete";
            comboBox2.Enabled = false;
            new_bug.Enabled = false;
            bugdetail.Visible = false;

            //this.Controls.Clear();
            //generate_report report = new generate_report();
            //report.TopLevel = false;
            //report.FormBorderStyle = FormBorderStyle.None;
            //this.Controls.Add(report);
            //report.Show();

            gen_report();

        } // (end generate bug report)

        private void save_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = "C:"; // default save to C:
            saveFileDialog1.Title = "Save as"; // save dialog title
            saveFileDialog1.FileName = ""; // save file name
            saveFileDialog1.Filter = "Excel File((97-2003)|*.xls|Excel File(2010)|*.xlsx"; // file save type

            if (saveFileDialog1.ShowDialog() != DialogResult.Cancel)
            {
                Excel = new Microsoft.Office.Interop.Excel.Application();
                Excel.Application.Workbooks.Add(Type.Missing);
                Excel.Columns.ColumnWidth = 20;
            } // end if

            for (int i = 1; i < dataGridView2.Columns.Count + 1; i++)
            {
                Excel.Cells[1, i] = dataGridView2.Columns[i - 1].HeaderText;
            } // end for (get the column)

            for (int i = 0; i < dataGridView2.Rows.Count; i++)
            {
                for (int j = 0; j < dataGridView2.Columns.Count; j++)
                {
                    Excel.Cells[i + 2, j + 1] = dataGridView2.Rows[i].Cells[j].Value.ToString();
                } // end for 
            } // end for

            Excel.Columns[9].Delete();
            Excel.Columns[10].Delete();

            Excel.ActiveWorkbook.SaveCopyAs(saveFileDialog1.FileName.ToString());
            Excel.ActiveWorkbook.Saved = true;
            Excel.Quit();
        } // (end save function) save data view data to Excel 

        private void gen_report()
        {
            complete = 1;
            adapter = new MySqlDataAdapter("select * from assignment2.bug where complete = '" + complete + "'", connection);

            connection.Open();
            DataSet ds = new DataSet();
            adapter.Fill(ds, "bug");
            dataGridView2.DataSource = ds.Tables["bug"];

            dataGridView2.AllowUserToAddRows = false;

            connection.Close();

            bug_detail = new ArrayList();
            bug_detail.Add("Bug ID : ");
            bug_detail.Add("Bug Title : ");
            bug_detail.Add("Build Version : ");
            bug_detail.Add("Severity : ");
            bug_detail.Add("Symptom : ");
            bug_detail.Add("Description : ");
            bug_detail.Add("Reproduction : ");
            bug_detail.Add("Summary : ");
            bug_detail.Add("Freeback");

            show_report();
        } // end generate report

        private void show_report ()
        {
            for (int x = 0; x < dataGridView2.Rows.Count; x++)
            {
                label = new Label[n];

                for (int i = 0; i < n; i++)
                {
                    label[i] = new Label();
                    label[i].Name = "n" + i;
                    label[i].Text = bug_detail[i].ToString();

                }

                for (int i = 0; i < n; i++)
                {
                    label[i].Visible = true;
                    label[i].Location = new Point(50, 30 + space);
                    bug_report.Controls.Add(label[i]);
                    space += 30;
                }

                detail_label = new Label[n];
                
                for (int i = 0; i < n; i++)
                {
                    detail_label[i] = new Label();
                    detail_label[i].Name = "n2" + i;

                    detail_label[i].Text = dataGridView2.Rows[x].Cells[i].Value.ToString();
                }

                for (int i = 0; i < n; i++)
                {
                    detail_label[i].Visible = true;
                    detail_label[i].Location = new Point(200, 30 + space2);
                    bug_report.Controls.Add(detail_label[i]);
                    space2 += 30;
                }

                space2 += 50;
                space += 50;
            }
        } // end show report

        private void backbtn_Click(object sender, EventArgs e)
        {
            bug_report.Visible = false;
            dataGridView1.Visible = true;
            bugreportsearch.Visible = true;
            txtserach.Visible = true;
            serachbtn.Visible = true;
            reflash.Visible = true;
            generate_report.Visible = true;
            searchRequest.Visible = true;
            comboBox2.Text = "Incomplete";
            comboBox2.Enabled = true;
            new_bug.Enabled = true;

            //bug_report.Controls.Clear();
            space = 20;
            space2 = 20;
        } // end back button

        private void complete_data ()
        {
            bugid.Text = row.Cells["bug_id"].Value.ToString();
            bugtitle.Text = row.Cells["title"].Value.ToString();
            bugver.Text = row.Cells["build_version"].Value.ToString();
            bugser.Text = row.Cells["severity"].Value.ToString();
            bugD.Text = row.Cells["description"].Value.ToString();
            bugR.Text = row.Cells["reproduction"].Value.ToString();
            bugS.Text = row.Cells["summary"].Value.ToString();
            bugSymptom.Text = row.Cells["symptom"].Value.ToString();
            bugFreeback.Text = row.Cells["freeback"].Value.ToString(); // text view get the database data

            connection.Open();
            string count_developer = "select count(department) from assignment2.bug;"; // count the developer
            cmd = new MySqlCommand(count_developer, connection);
            string count = cmd.ExecuteScalar().ToString();
            int Dcount = Int32.Parse(count); // change the string to int
            
            comboSource = new Dictionary<int, string>();
            array = new ArrayList();
            for (int i = 1; i <= Dcount; i++)
            {
                string sql = "select name from assignment2.login where id = '"+ i +"' and department ='3';";
                cmd = new MySqlCommand(sql, connection);
                MySqlDataReader reader = cmd.ExecuteReader();
                
                if (reader.Read())
                {
                    array.Add(reader.GetString("name"));
                } // end if
                reader.Close();  // reader close
            } // end for
            comboBox1.DataSource = array; // developer name combo box data source use array
            connection.Close();
        } // (end complete)  input the database data to text view

        private void reset_data ()
        {
            newBugDescription.Text = null;
            newBugSeverity.Text = "1";
            newBugSymptom.Text = null;
            newBugTitle.Text = null;
        } // (end reset)  reset text view function

        private void update_data ()
        {
            connection.Open();
            MySqlCommand cmd = new MySqlCommand(send, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
            data(); // show the data
        } // (end update date)  push button can reflash data

        private void edit_detail()
        {
            complete_data();
            bugtitle.ReadOnly = false;
            bugser.Enabled = true;
            bugser.DropDownStyle = ComboBoxStyle.DropDownList;
            bugS.ReadOnly = false;
            bugR.ReadOnly = false;
            bugD.ReadOnly = false;
            bugSymptom.ReadOnly = false;
        } // change all bug report detail can edit

        private void detail_readonly()
        {
            complete_data();
            bugtitle.ReadOnly = true;
            bugser.Enabled = false;
            bugser.DropDownStyle = ComboBoxStyle.DropDownList;
            bugS.ReadOnly = true;
            bugR.ReadOnly = true;
            bugD.ReadOnly = true;
            bugver.ReadOnly = true;
            bugSymptom.ReadOnly = true;
            bugFreeback.ReadOnly = true;


            this.finish.Visible = false;
            this.update.Visible = false;
            this.returnToQA.Visible = false;
            this.assigns.Visible = false;
            this.dm_plugin.Visible = false;
        } // all the bug report detail are read only

        private void mouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        } // (end mouse click)  this is click the data view cell and then highlight the row

        private void dataformat(object sender, DataGridViewCellFormattingEventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
                if (Convert.ToInt32(row.Cells[6].Value) == 5)
                {
                    row.DefaultCellStyle.ForeColor = Color.Red;
                }
                else if (Convert.ToInt32(row.Cells[6].Value) == 4)
                {
                    row.DefaultCellStyle.ForeColor = Color.IndianRed;
                }
                else if (Convert.ToInt32(row.Cells[6].Value) == 3)
                {
                    row.DefaultCellStyle.ForeColor = Color.Orange;
                }
                else if (Convert.ToInt32(row.Cells[6].Value) == 2)
                {
                    row.DefaultCellStyle.ForeColor = Color.Gold;
                }
                else
                {
                    row.DefaultCellStyle.ForeColor = Color.Black;
                } // end if
        } // (end dataformat)  count the severity level and show different color
    } // end class
} // end namespace
