﻿namespace assignment2
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.time = new System.Windows.Forms.Label();
            this.id = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.user_name = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.new_bug = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.bugdetail = new System.Windows.Forms.Panel();
            this.bugser = new System.Windows.Forms.ComboBox();
            this.dm_plugin = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.bugFreeback = new System.Windows.Forms.RichTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.update = new System.Windows.Forms.Button();
            this.bugid = new System.Windows.Forms.TextBox();
            this.bugS = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.bugR = new System.Windows.Forms.RichTextBox();
            this.bugD = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.bugtitle = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.bugver = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.returnToQA = new System.Windows.Forms.Button();
            this.finish = new System.Windows.Forms.Button();
            this.assigns = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.newBugSymptom = new System.Windows.Forms.RichTextBox();
            this.newBugReproduction = new System.Windows.Forms.RichTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.submit = new System.Windows.Forms.Button();
            this.reset = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.newBugDescription = new System.Windows.Forms.RichTextBox();
            this.newBugSeverity = new System.Windows.Forms.ComboBox();
            this.newBugTitle = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.bugreportsearch = new System.Windows.Forms.Label();
            this.txtserach = new System.Windows.Forms.TextBox();
            this.serachbtn = new System.Windows.Forms.Button();
            this.searchRequest = new System.Windows.Forms.ComboBox();
            this.reflash = new System.Windows.Forms.Button();
            this.generate_report = new System.Windows.Forms.Button();
            this.bug_report = new System.Windows.Forms.Panel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.backbtn = new System.Windows.Forms.Button();
            this.save = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label14 = new System.Windows.Forms.Label();
            this.newBugSummary = new System.Windows.Forms.RichTextBox();
            this.bugSymptom = new System.Windows.Forms.RichTextBox();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.bugdetail.SuspendLayout();
            this.dm_plugin.SuspendLayout();
            this.panel5.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.bug_report.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 100);
            this.panel2.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.time);
            this.panel3.Controls.Add(this.id);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.user_name);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.new_bug);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 807);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1176, 36);
            this.panel3.TabIndex = 2;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label20.Location = new System.Drawing.Point(630, 6);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(93, 28);
            this.label20.TabIndex = 6;
            this.label20.Text = "Time : ";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // time
            // 
            this.time.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.time.Location = new System.Drawing.Point(720, 6);
            this.time.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(294, 28);
            this.time.TabIndex = 5;
            this.time.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // id
            // 
            this.id.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.id.Location = new System.Drawing.Point(471, 6);
            this.id.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.id.Name = "id";
            this.id.Size = new System.Drawing.Size(150, 28);
            this.id.TabIndex = 4;
            this.id.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(327, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 28);
            this.label3.TabIndex = 3;
            this.label3.Text = "User ID : ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // user_name
            // 
            this.user_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.user_name.Location = new System.Drawing.Point(176, 6);
            this.user_name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.user_name.Name = "user_name";
            this.user_name.Size = new System.Drawing.Size(150, 28);
            this.user_name.TabIndex = 2;
            this.user_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(20, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "User Name : ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // new_bug
            // 
            this.new_bug.Location = new System.Drawing.Point(1022, 2);
            this.new_bug.Margin = new System.Windows.Forms.Padding(4);
            this.new_bug.Name = "new_bug";
            this.new_bug.Size = new System.Drawing.Size(112, 34);
            this.new_bug.TabIndex = 0;
            this.new_bug.Text = "New Bug";
            this.new_bug.UseVisualStyleBackColor = true;
            this.new_bug.Click += new System.EventHandler(this.new_bug_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(8, 84);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1164, 225);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataformat);
            this.dataGridView1.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.mouseClick);
            // 
            // bugdetail
            // 
            this.bugdetail.BackColor = System.Drawing.Color.Transparent;
            this.bugdetail.Controls.Add(this.bugSymptom);
            this.bugdetail.Controls.Add(this.bugser);
            this.bugdetail.Controls.Add(this.dm_plugin);
            this.bugdetail.Controls.Add(this.bugFreeback);
            this.bugdetail.Controls.Add(this.label17);
            this.bugdetail.Controls.Add(this.label15);
            this.bugdetail.Controls.Add(this.update);
            this.bugdetail.Controls.Add(this.bugid);
            this.bugdetail.Controls.Add(this.bugS);
            this.bugdetail.Controls.Add(this.label10);
            this.bugdetail.Controls.Add(this.label9);
            this.bugdetail.Controls.Add(this.bugR);
            this.bugdetail.Controls.Add(this.bugD);
            this.bugdetail.Controls.Add(this.label7);
            this.bugdetail.Controls.Add(this.label6);
            this.bugdetail.Controls.Add(this.label5);
            this.bugdetail.Controls.Add(this.bugtitle);
            this.bugdetail.Controls.Add(this.label4);
            this.bugdetail.Controls.Add(this.bugver);
            this.bugdetail.Controls.Add(this.label2);
            this.bugdetail.Controls.Add(this.returnToQA);
            this.bugdetail.Controls.Add(this.finish);
            this.bugdetail.Controls.Add(this.assigns);
            this.bugdetail.Location = new System.Drawing.Point(0, 306);
            this.bugdetail.Margin = new System.Windows.Forms.Padding(4);
            this.bugdetail.Name = "bugdetail";
            this.bugdetail.Size = new System.Drawing.Size(1176, 498);
            this.bugdetail.TabIndex = 1;
            this.bugdetail.Visible = false;
            // 
            // bugser
            // 
            this.bugser.FormattingEnabled = true;
            this.bugser.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.bugser.Location = new System.Drawing.Point(214, 159);
            this.bugser.Margin = new System.Windows.Forms.Padding(4);
            this.bugser.Name = "bugser";
            this.bugser.Size = new System.Drawing.Size(120, 26);
            this.bugser.TabIndex = 26;
            this.bugser.Text = "1";
            // 
            // dm_plugin
            // 
            this.dm_plugin.Controls.Add(this.label18);
            this.dm_plugin.Controls.Add(this.comboBox1);
            this.dm_plugin.Location = new System.Drawing.Point(20, 320);
            this.dm_plugin.Margin = new System.Windows.Forms.Padding(4);
            this.dm_plugin.Name = "dm_plugin";
            this.dm_plugin.Size = new System.Drawing.Size(442, 45);
            this.dm_plugin.TabIndex = 25;
            this.dm_plugin.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(6, 4);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(231, 29);
            this.label18.TabIndex = 1;
            this.label18.Text = "Developer Name : ";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(248, 4);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(180, 26);
            this.comboBox1.TabIndex = 0;
            // 
            // bugFreeback
            // 
            this.bugFreeback.Location = new System.Drawing.Point(483, 393);
            this.bugFreeback.Margin = new System.Windows.Forms.Padding(4);
            this.bugFreeback.Name = "bugFreeback";
            this.bugFreeback.Size = new System.Drawing.Size(595, 88);
            this.bugFreeback.TabIndex = 24;
            this.bugFreeback.Text = "";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(480, 370);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(86, 18);
            this.label17.TabIndex = 23;
            this.label17.Text = "Freeback : ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(40, 201);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(132, 29);
            this.label15.TabIndex = 21;
            this.label15.Text = "Symptom : ";
            // 
            // update
            // 
            this.update.Location = new System.Drawing.Point(156, 430);
            this.update.Margin = new System.Windows.Forms.Padding(4);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(105, 34);
            this.update.TabIndex = 17;
            this.update.Text = "Send";
            this.update.UseVisualStyleBackColor = true;
            this.update.Click += new System.EventHandler(this.update_Click);
            // 
            // bugid
            // 
            this.bugid.Location = new System.Drawing.Point(214, 38);
            this.bugid.Margin = new System.Windows.Forms.Padding(4);
            this.bugid.Name = "bugid";
            this.bugid.ReadOnly = true;
            this.bugid.Size = new System.Drawing.Size(120, 29);
            this.bugid.TabIndex = 16;
            // 
            // bugS
            // 
            this.bugS.Location = new System.Drawing.Point(480, 276);
            this.bugS.Margin = new System.Windows.Forms.Padding(4);
            this.bugS.Name = "bugS";
            this.bugS.Size = new System.Drawing.Size(598, 88);
            this.bugS.TabIndex = 15;
            this.bugS.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(480, 254);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 18);
            this.label10.TabIndex = 14;
            this.label10.Text = "Summary : ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(480, 135);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 18);
            this.label9.TabIndex = 13;
            this.label9.Text = "Reproduction : ";
            // 
            // bugR
            // 
            this.bugR.Location = new System.Drawing.Point(480, 159);
            this.bugR.Margin = new System.Windows.Forms.Padding(4);
            this.bugR.Name = "bugR";
            this.bugR.Size = new System.Drawing.Size(598, 88);
            this.bugR.TabIndex = 12;
            this.bugR.Text = "";
            // 
            // bugD
            // 
            this.bugD.Location = new System.Drawing.Point(480, 44);
            this.bugD.Name = "bugD";
            this.bugD.Size = new System.Drawing.Size(598, 88);
            this.bugD.TabIndex = 7;
            this.bugD.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(480, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 18);
            this.label7.TabIndex = 6;
            this.label7.Text = "Description : ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(38, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 29);
            this.label6.TabIndex = 5;
            this.label6.Text = "Severity :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(40, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(174, 29);
            this.label5.TabIndex = 4;
            this.label5.Text = "Build Version : ";
            // 
            // bugtitle
            // 
            this.bugtitle.Location = new System.Drawing.Point(214, 76);
            this.bugtitle.Name = "bugtitle";
            this.bugtitle.Size = new System.Drawing.Size(120, 29);
            this.bugtitle.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(40, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 29);
            this.label4.TabIndex = 2;
            this.label4.Text = "Bug Title : ";
            // 
            // bugver
            // 
            this.bugver.Location = new System.Drawing.Point(214, 117);
            this.bugver.Name = "bugver";
            this.bugver.ReadOnly = true;
            this.bugver.Size = new System.Drawing.Size(120, 29);
            this.bugver.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(40, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 29);
            this.label2.TabIndex = 0;
            this.label2.Text = "Bug ID : ";
            // 
            // returnToQA
            // 
            this.returnToQA.Location = new System.Drawing.Point(141, 388);
            this.returnToQA.Name = "returnToQA";
            this.returnToQA.Size = new System.Drawing.Size(120, 34);
            this.returnToQA.TabIndex = 19;
            this.returnToQA.Text = "Return to QA";
            this.returnToQA.UseVisualStyleBackColor = true;
            this.returnToQA.Visible = false;
            this.returnToQA.Click += new System.EventHandler(this.returnToQA_Click);
            // 
            // finish
            // 
            this.finish.Location = new System.Drawing.Point(270, 430);
            this.finish.Margin = new System.Windows.Forms.Padding(4);
            this.finish.Name = "finish";
            this.finish.Size = new System.Drawing.Size(105, 34);
            this.finish.TabIndex = 18;
            this.finish.Text = "Finish";
            this.finish.UseVisualStyleBackColor = true;
            this.finish.Click += new System.EventHandler(this.finish_Click);
            // 
            // assigns
            // 
            this.assigns.Location = new System.Drawing.Point(270, 388);
            this.assigns.Name = "assigns";
            this.assigns.Size = new System.Drawing.Size(105, 34);
            this.assigns.TabIndex = 20;
            this.assigns.Text = "Assigns";
            this.assigns.UseVisualStyleBackColor = true;
            this.assigns.Visible = false;
            this.assigns.Click += new System.EventHandler(this.assigns_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel5.Controls.Add(this.newBugSummary);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.newBugSymptom);
            this.panel5.Controls.Add(this.newBugReproduction);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.submit);
            this.panel5.Controls.Add(this.reset);
            this.panel5.Controls.Add(this.back);
            this.panel5.Controls.Add(this.newBugDescription);
            this.panel5.Controls.Add(this.newBugSeverity);
            this.panel5.Controls.Add(this.newBugTitle);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Location = new System.Drawing.Point(0, 316);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1176, 488);
            this.panel5.TabIndex = 2;
            this.panel5.Visible = false;
            // 
            // newBugSymptom
            // 
            this.newBugSymptom.Location = new System.Drawing.Point(78, 230);
            this.newBugSymptom.Name = "newBugSymptom";
            this.newBugSymptom.Size = new System.Drawing.Size(410, 142);
            this.newBugSymptom.TabIndex = 19;
            this.newBugSymptom.Text = "";
            // 
            // newBugReproduction
            // 
            this.newBugReproduction.Location = new System.Drawing.Point(573, 240);
            this.newBugReproduction.Margin = new System.Windows.Forms.Padding(4);
            this.newBugReproduction.Name = "newBugReproduction";
            this.newBugReproduction.Size = new System.Drawing.Size(493, 142);
            this.newBugReproduction.TabIndex = 18;
            this.newBugReproduction.Text = "";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(75, 200);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(87, 18);
            this.label21.TabIndex = 17;
            this.label21.Text = "Symptom : ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(570, 218);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(114, 18);
            this.label19.TabIndex = 16;
            this.label19.Text = "Reproduction : ";
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(340, 410);
            this.submit.Margin = new System.Windows.Forms.Padding(4);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(112, 34);
            this.submit.TabIndex = 13;
            this.submit.Text = "Submit";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.submit_Click);
            // 
            // reset
            // 
            this.reset.Location = new System.Drawing.Point(40, 410);
            this.reset.Margin = new System.Windows.Forms.Padding(4);
            this.reset.Name = "reset";
            this.reset.Size = new System.Drawing.Size(112, 34);
            this.reset.TabIndex = 12;
            this.reset.Text = "Reset";
            this.reset.UseVisualStyleBackColor = true;
            this.reset.Click += new System.EventHandler(this.reset_Click);
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(190, 410);
            this.back.Margin = new System.Windows.Forms.Padding(4);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(112, 34);
            this.back.TabIndex = 11;
            this.back.Text = "Back";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // newBugDescription
            // 
            this.newBugDescription.Location = new System.Drawing.Point(573, 64);
            this.newBugDescription.Margin = new System.Windows.Forms.Padding(4);
            this.newBugDescription.Name = "newBugDescription";
            this.newBugDescription.Size = new System.Drawing.Size(493, 142);
            this.newBugDescription.TabIndex = 10;
            this.newBugDescription.Text = "";
            // 
            // newBugSeverity
            // 
            this.newBugSeverity.FormattingEnabled = true;
            this.newBugSeverity.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.newBugSeverity.Location = new System.Drawing.Point(170, 112);
            this.newBugSeverity.Margin = new System.Windows.Forms.Padding(4);
            this.newBugSeverity.Name = "newBugSeverity";
            this.newBugSeverity.Size = new System.Drawing.Size(148, 26);
            this.newBugSeverity.TabIndex = 14;
            this.newBugSeverity.Text = "1";
            // 
            // newBugTitle
            // 
            this.newBugTitle.Location = new System.Drawing.Point(170, 70);
            this.newBugTitle.Margin = new System.Windows.Forms.Padding(4);
            this.newBugTitle.Name = "newBugTitle";
            this.newBugTitle.Size = new System.Drawing.Size(148, 29);
            this.newBugTitle.TabIndex = 6;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(570, 42);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(103, 18);
            this.label16.TabIndex = 5;
            this.label16.Text = "Description : ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(75, 120);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 18);
            this.label13.TabIndex = 2;
            this.label13.Text = "Severity : ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(75, 160);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(371, 18);
            this.label12.TabIndex = 1;
            this.label12.Text = "(5 severity = high priority, 1 severity = low priority)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(75, 75);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 18);
            this.label11.TabIndex = 0;
            this.label11.Text = "Bug Title : ";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1176, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(71, 27);
            this.toolStripMenuItem1.Text = "menu";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(162, 30);
            this.toolStripMenuItem2.Text = "Log Out";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Incomplete",
            "Complete"});
            this.comboBox2.Location = new System.Drawing.Point(904, 3);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(180, 26);
            this.comboBox2.TabIndex = 5;
            this.comboBox2.Tag = "";
            this.comboBox2.Text = "Incomplete";
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // bugreportsearch
            // 
            this.bugreportsearch.AutoSize = true;
            this.bugreportsearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.bugreportsearch.Location = new System.Drawing.Point(6, 42);
            this.bugreportsearch.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bugreportsearch.Name = "bugreportsearch";
            this.bugreportsearch.Size = new System.Drawing.Size(109, 29);
            this.bugreportsearch.TabIndex = 6;
            this.bugreportsearch.Text = "Search :";
            this.bugreportsearch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtserach
            // 
            this.txtserach.Location = new System.Drawing.Point(114, 40);
            this.txtserach.Margin = new System.Windows.Forms.Padding(4);
            this.txtserach.Name = "txtserach";
            this.txtserach.Size = new System.Drawing.Size(148, 29);
            this.txtserach.TabIndex = 7;
            this.txtserach.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keydown);
            // 
            // serachbtn
            // 
            this.serachbtn.Location = new System.Drawing.Point(477, 40);
            this.serachbtn.Margin = new System.Windows.Forms.Padding(4);
            this.serachbtn.Name = "serachbtn";
            this.serachbtn.Size = new System.Drawing.Size(112, 34);
            this.serachbtn.TabIndex = 8;
            this.serachbtn.Text = "Search";
            this.serachbtn.UseVisualStyleBackColor = true;
            this.serachbtn.Click += new System.EventHandler(this.serachbtn_Click);
            // 
            // searchRequest
            // 
            this.searchRequest.FormattingEnabled = true;
            this.searchRequest.Items.AddRange(new object[] {
            "Bug ID",
            "Title",
            "Severity",
            "Developer Name"});
            this.searchRequest.Location = new System.Drawing.Point(274, 42);
            this.searchRequest.Margin = new System.Windows.Forms.Padding(4);
            this.searchRequest.Name = "searchRequest";
            this.searchRequest.Size = new System.Drawing.Size(180, 26);
            this.searchRequest.TabIndex = 9;
            this.searchRequest.Text = "Title";
            // 
            // reflash
            // 
            this.reflash.Location = new System.Drawing.Point(598, 40);
            this.reflash.Margin = new System.Windows.Forms.Padding(4);
            this.reflash.Name = "reflash";
            this.reflash.Size = new System.Drawing.Size(112, 34);
            this.reflash.TabIndex = 10;
            this.reflash.Text = "Reflash";
            this.reflash.UseVisualStyleBackColor = true;
            this.reflash.Click += new System.EventHandler(this.reflash_Click);
            // 
            // generate_report
            // 
            this.generate_report.Location = new System.Drawing.Point(722, 42);
            this.generate_report.Margin = new System.Windows.Forms.Padding(4);
            this.generate_report.Name = "generate_report";
            this.generate_report.Size = new System.Drawing.Size(186, 34);
            this.generate_report.TabIndex = 11;
            this.generate_report.Text = "Generate Bug Report";
            this.generate_report.UseVisualStyleBackColor = true;
            this.generate_report.Click += new System.EventHandler(this.generate_report_Click);
            // 
            // bug_report
            // 
            this.bug_report.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.bug_report.Controls.Add(this.dataGridView2);
            this.bug_report.Controls.Add(this.backbtn);
            this.bug_report.Controls.Add(this.save);
            this.bug_report.Location = new System.Drawing.Point(0, 40);
            this.bug_report.Margin = new System.Windows.Forms.Padding(4);
            this.bug_report.Name = "bug_report";
            this.bug_report.Size = new System.Drawing.Size(1172, 764);
            this.bug_report.TabIndex = 12;
            this.bug_report.Visible = false;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(0, 2);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(908, 225);
            this.dataGridView2.TabIndex = 2;
            this.dataGridView2.Visible = false;
            // 
            // backbtn
            // 
            this.backbtn.Location = new System.Drawing.Point(1022, 4);
            this.backbtn.Margin = new System.Windows.Forms.Padding(4);
            this.backbtn.Name = "backbtn";
            this.backbtn.Size = new System.Drawing.Size(112, 34);
            this.backbtn.TabIndex = 1;
            this.backbtn.Text = "Back";
            this.backbtn.UseVisualStyleBackColor = true;
            this.backbtn.Click += new System.EventHandler(this.backbtn_Click);
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(873, 4);
            this.save.Margin = new System.Windows.Forms.Padding(4);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(141, 34);
            this.save.TabIndex = 0;
            this.save.Text = "Save as Excel";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(570, 395);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 18);
            this.label14.TabIndex = 20;
            this.label14.Text = "Summary : ";
            // 
            // newBugSummary
            // 
            this.newBugSummary.Location = new System.Drawing.Point(573, 417);
            this.newBugSummary.Margin = new System.Windows.Forms.Padding(4);
            this.newBugSummary.Name = "newBugSummary";
            this.newBugSummary.Size = new System.Drawing.Size(493, 142);
            this.newBugSummary.TabIndex = 21;
            this.newBugSummary.Text = "";
            // 
            // bugSymptom
            // 
            this.bugSymptom.Location = new System.Drawing.Point(45, 233);
            this.bugSymptom.Name = "bugSymptom";
            this.bugSymptom.Size = new System.Drawing.Size(380, 70);
            this.bugSymptom.TabIndex = 27;
            this.bugSymptom.Text = "";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1176, 843);
            this.Controls.Add(this.generate_report);
            this.Controls.Add(this.reflash);
            this.Controls.Add(this.searchRequest);
            this.Controls.Add(this.serachbtn);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.txtserach);
            this.Controls.Add(this.bugreportsearch);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.bugdetail);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.bug_report);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "main";
            this.Text = "Bug Tracking System";
            this.Load += new System.EventHandler(this.main_Load);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.bugdetail.ResumeLayout(false);
            this.bugdetail.PerformLayout();
            this.dm_plugin.ResumeLayout(false);
            this.dm_plugin.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.bug_report.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button new_bug;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label id;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label user_name;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel bugdetail;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.TextBox bugtitle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox bugver;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox bugD;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox bugR;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox bugS;
        private System.Windows.Forms.TextBox bugid;
        private System.Windows.Forms.Button update;
        private System.Windows.Forms.Button finish;
        private System.Windows.Forms.Label time;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RichTextBox newBugDescription;
        private System.Windows.Forms.TextBox newBugTitle;
        private System.Windows.Forms.ComboBox newBugSeverity;
        private System.Windows.Forms.Button submit;
        private System.Windows.Forms.Button reset;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button returnToQA;
        private System.Windows.Forms.Button assigns;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RichTextBox bugFreeback;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel dm_plugin;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label bugreportsearch;
        private System.Windows.Forms.TextBox txtserach;
        private System.Windows.Forms.Button serachbtn;
        private System.Windows.Forms.ComboBox bugser;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox searchRequest;
        private System.Windows.Forms.Button reflash;
        private System.Windows.Forms.Button generate_report;
        private System.Windows.Forms.Panel bug_report;
        private System.Windows.Forms.Button backbtn;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.RichTextBox newBugReproduction;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.RichTextBox newBugSymptom;
        private System.Windows.Forms.RichTextBox newBugSummary;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RichTextBox bugSymptom;
    }
}