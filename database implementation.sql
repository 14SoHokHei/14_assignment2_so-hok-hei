-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 產生時間： 2016-01-11 14:01:32
-- 伺服器版本: 10.1.9-MariaDB
-- PHP 版本： 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `assignment2`
--

-- --------------------------------------------------------

--
-- 資料表結構 `bug`
--

CREATE TABLE `bug` (
  `bug_id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `build_version` int(10) NOT NULL DEFAULT '1',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `reproduction` text COLLATE utf8_unicode_ci NOT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `severity` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `symptom` text COLLATE utf8_unicode_ci NOT NULL,
  `complete` int(11) NOT NULL DEFAULT '0',
  `department` int(11) NOT NULL,
  `freeback` text COLLATE utf8_unicode_ci NOT NULL,
  `developer_name` text COLLATE utf8_unicode_ci NOT NULL,
  `start_date` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `end_date` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `bug`
--

INSERT INTO `bug` (`bug_id`, `title`, `build_version`, `description`, `reproduction`, `summary`, `severity`, `priority`, `symptom`, `complete`, `department`, `freeback`, `developer_name`, `start_date`, `end_date`) VALUES
(1001, 'test', 1, 'xxxxxx', 'xxxxxx', '', 5, 5, '2', 0, 1, 'abc', 'tom', '', ''),
(1002, 'error', 0, 'xxxxx', '', '', 2, 2, '0', 1, 1, '', '', '', ''),
(1003, 'error', 0, 'xxxxx', '', '', 2, 2, '0', 0, 1, '', '', '', ''),
(1004, 'error', 5, 'xxxxxnbjbjb', 'nbjbj', 'n nbj', 2, 2, '0', 0, 3, '', 'tom', '', ''),
(1005, 'error', 0, '', '', '', 2, 2, '0', 1, 0, '', '', '', ''),
(1006, 'xxxx', 2, 'xxxx', '', '', 4, 2, '123', 0, 0, '', '', '', ''),
(1007, 'xxxx', 1, 'xxxx', '', '', 4, 5, '', 0, 0, '', '', '', ''),
(1008, 'xxxx', 1, 'xxxx', 'xxxx', '', 5, 5, 'xxxx', 0, 0, '', 'tom', '', ''),
(1009, 'new', 1, 'xxxx', '', '', 5, 5, 'xxxx', 0, 0, '', 'mary', '', ''),
(1010, 'zzz', 1, 'zzz', 'vvvv', '', 5, 5, 'zzz', 0, 0, '', '', '', ''),
(1011, 'zzzz', 1, 'xxxx', '', '', 4, 4, 'zzzz', 1, 0, '', '', '', ''),
(1012, 'new', 1, 'xxxx', '', '', 5, 5, 'zzzzxxx', 1, 0, '', 'mary', '', ''),
(1013, '', 1, '', '', '', 1, 1, '', 0, 2, '', '', '', ''),
(1014, 'newbug', 1, 'test', '', '', 5, 1, 'test', 0, 2, '', '', '', ''),
(1015, '1234', 1, 'xxxx', '', '', 1, 0, 'xxxx', 0, 2, '', '', '11/01/2016', '');

-- --------------------------------------------------------

--
-- 資料表結構 `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `department`
--

INSERT INTO `department` (`id`, `name`) VALUES
(1, 'qa'),
(2, 'development manager'),
(3, 'developer');

-- --------------------------------------------------------

--
-- 資料表結構 `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `pwd` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `department` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `login`
--

INSERT INTO `login` (`id`, `user_id`, `name`, `pwd`, `department`) VALUES
(1, 20150214, 'peter', '0000', 1),
(2, 20151225, 'jan', '0000', 2),
(3, 20121115, 'tom', '0000', 3),
(4, 20150430, 'mary', '0000', 3);

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `bug`
--
ALTER TABLE `bug`
  ADD PRIMARY KEY (`bug_id`),
  ADD KEY `department` (`department`);

--
-- 資料表索引 `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department` (`department`);

--
-- 已匯出資料表的限制(Constraint)
--

--
-- 資料表的 Constraints `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `login_ibfk_1` FOREIGN KEY (`department`) REFERENCES `department` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
